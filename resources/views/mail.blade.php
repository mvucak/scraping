<!DOCTYPE html>
<html>
    <head>
        <title>Mail</title>

        <link href="//fonts.googleapis.com/css?family=Lato:100" rel="stylesheet" type="text/css">
        <style>
            html, body {
                height: 100%;
            }

            body {
                margin: 0;
                padding: 0;
                width: 100%;
                display: table;
                font-weight: 100;
                font-family: 'Lato';
            }

            .container {
                text-align: center;
                display: table-cell;
                vertical-align: middle;
            }

            .content {
                text-align: center;
                display: inline-block;
            }

            .title {
                font-size: 96px;
            }
            
            .values{
                display: block;
                margin-top: 10px;
            }
            .item{
                display: block;
                margin-bottom: 5px;
            }
        </style>
    </head>
    <body>
        <div class="container">
            <div class="content">
                <div class="title">Rezultati pretrage za {{ $search }}</div>
                <div class="values">
                    @foreach ($res as $r)
                        <div class="item">
                        @foreach ($r as $a)
                            {{$a}}
                        @endforeach
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    </body>
</html>