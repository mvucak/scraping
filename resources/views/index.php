<html ng-app="searchApp">
	<head>
            <meta charset="UTF-8">
            <meta name="viewport" content="width=device-width, initial-scale=1.0, minimal-ui">
            <meta name="description" content="Usporedi cijene mobilnih uređaja.">
            <title>web servis za usporedbu</title>
            <link rel="stylesheet" href="css/bootstrap.min.css">
            <link rel="stylesheet" href="css/style.css">
	</head>
        <body ng-controller="searchController">
		<!-- Header -->
		<section class="header">
			<div class="container">
				<div class="row">
					<div class="col-md-8">
						<h1>Pronađi najbolju ponudu</h1>
						<p>Pronađite model i ponudu koji vama odgovaraju</p>
					</div>
				</div>
			</div>
		</section>


		<section class="main-content">
			<div class="container">
				<div class="row">

				<!--Main content div START-->
					<div class="col-md-8">
						<!--pretraga -->
						<div class="row">
							<div class="panel panel-default panel-red">
								<div class="panel-heading panel-red-heading">
							    	<h2 class="panel-title">Pretraga po nazivu modela</h2>
							    </div>
							    <div class="panel-body">
									<div class="col-md-12 model-search">
										<div class="col-md-4">
											<span>Upišite model mobitela</span>
										</div>	
										<div class="col-md-8">
											<div class="input-group">
												<input type="text" class="form-control" id="phone-model" ng-model="search">
											</div>
										</div>
									</div>	
								
									<div class="col-md-12">
										<div class="col-md-4">
										</div>	
										<div class="col-md-8">
											<button class="btn btn-default" type="submit" ng-click="onSearch()">Pretraži</button>
										</div>
									</div>
								</div>
							</div>				
						</div>
                                                <div class="row">    
                                                    <div class="col-md-12 valuta">
                                                        <div class="col-md-4">
                                                                <span>Odaberite valutu</span>
                                                        </div>	
                                                        <div class="col-md-8">
                                                                <select class="form-control" ng-model="currency_sel" ng-change="changeCurrency()">
                                                                        <option id="hrk" selected>HRK</option>
                                                                        <option id="eur">EUR</option>
                                                                        <option id="chf">CHF</option>
                                                                </select>
                                                        </div>
                                                    </div>
                                                </div>

						<div class="row" ng-show="show == 1">
							<div class="panel panel-default">
							  	<div class="panel-heading panel-heading-black">
							    	<h3 class="panel-title title-rezultati">Rezultati pretrage</h3>
                                                                    <div class="col-md-12">
									<p><span class="time-label">Vrijeme pretraživanja:</span> <span id="date-time" class="date-time"> {{searchResults.updated_at}}</span>
                                                                        <input type="email"  placeholder="email@email.com" class="form-control small" value="" ng-model="email">    
                                                                    	<span class="float-right"><button class="btn btn-default" type="submit" ng-click="onSentToEmail()" >{{sendMailTxt}}</button></span>
								    	</p>	
								    </div>
                                                                    <div class="clearfix"></div>
							  	</div>
							  	<div class="panel-body bdy" ng-repeat="item in searchResults.results | orderBy:'price_sum'">
                                                                    <div class="col-md-12" ng-class="$first ? 'best-value' : 'rest'">
                                                                            <div class="col-md-3">
                                                                                    <div class="input-group">
                                                                                            <span class="naziv-modela">{{item.provider}}</span>
                                                                                             <input type="text" class="form-control" value="{{item.price_sum * currency_multi| number:2}} {{currency_sel}}" disabled>
                                                                                            </div>
                                                                                    </div>
                                                                                    
                                                                            <div class="col-md-6">
                                                                                    <span class="naziv-modela">{{item.name}}</span>
                                                                                    <p>
                                                                                            <span class="label-cnt">Preplata: </span>
                                                                                        <span class="cnt"> {{item.price_monthly * currency_multi | number:2}} {{currency_sel}}/mj</span>
                                                                                    </p>
                                                                                    <p>   
                                                                                        <span class="label-cnt">Jednokratna uplata: </span>
                                                                                        <span class="cnt"> {{item.price_once * currency_multi | number:2}} {{currency_sel}}</span>
                                                                                    </p>
                                                                            </div>
                                                                            <div class="col-md-3" ng-show="$first">
                                                                                    <img src="img/best-deal.jpg" class="img-style img-responsive hidden-sm hidden-xs"/>
                                                                            </div>
                                                                    </div>
                                                                </div>    
                                                            
							</div> 	
							
						</div>
						<div class="row" ng-show="show == 2">
							<div class="col-md-12  searching">
								Pretražujem . . . 
							</div>	
						</div>
					</div>
					<!--Main content div END-->

					<!--div za razmak -->
					<div class="col-md-1">
					</div>

					<!--Right sidebar START-->
					<div class="col-md-3">
						<ul class="list-group">
							<li class="list-group-item li-title"><b>Posljednje pretrage<b></li>
                                                        <span ng-repeat="h in history">
                                                            <li class="list-group-item" ng-click="onHistory(h.id)"><a href="#">{{h.name}}</a></li>
                                                        </span>
						</ul>
					</div>
					<!--Right sidebar END-->


				</div>
			</div>
		<section>
	</body>
        
        <script src="js/jquery-1.11.3.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/angular.min.js"></script>
        <script src="js/angular-route.min.js"></script>
        
        <script src="js/app.js"></script>
        <script src="js/services/currency.js"></script>
        <script src="js/services/history.js"></script>
        <script src="js/services/search.js"></script>
        <script src="js/services/email.js"></script>
</html>
