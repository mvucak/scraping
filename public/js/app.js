
var searchApp = angular.module('searchApp', []);
 
searchApp.controller('searchController', ['$scope', 'CurrencyService', 'HistoryService', 'SearchService', 'EmailService',
                        function($scope, CurrencyService, HistoryService, SearchService, EmailService) 
{
    $scope.search           = '';       // string za pretragu
    $scope.currency_sel     = "HRK";    // odabrana valuta
    $scope.currency_multi   = 1;        // multiplikator za valutu
    $scope.currency         = [];       // odnosi valuta prema kuni
    $scope.history          = [];       // povijest pretraživanja
    $scope.show             = 0;        // 1 - prikazuje rezultate pretrage, 2 - prikazuje loading ekran(prilikom searcha)
    $scope.sendMailTxt      = "Pošalji na e-mail"; // text za slanje emaila
    $scope.searchResults    = [];       // rezultati pretraživanja
    
    // učitaj početne podatke 
    loadRemoteData();
  
    // pretraživanje
    $scope.onSearch         = function onSearch(){
        $scope.sendMailTxt      = "Pošalji na e-mail";
        searchData();
    };
  
    // prikaz nekog od prethodnih pretraživanja
    $scope.onHistory        = function onHistory(id){
        $scope.sendMailTxt      = "Pošalji na e-mail";
        getHistory(id);
    };
    
    // promijena valute
    $scope.changeCurrency   = function onSearch(){
        $scope.sendMailTxt      = "Pošalji na e-mail";
        if($scope.currency_sel == "EUR")
            $scope.currency_multi = $scope.currency.EUR;
        else if($scope.currency_sel == "CHF")
            $scope.currency_multi = $scope.currency.CHF;
        else
            $scope.currency_multi = 1;
    };

    // slanje na mail
    $scope.onSentToEmail = function onSentToEmail(){
        sendEmail($scope.searchResults.search, $scope.email, $scope.searchResults.results, $scope.currency_multi, $scope.currency_sel);
    };

    // učitavanje na startu
    function loadRemoteData() {
        // valute
        loadCurrency()

        // povijest pretraživanja
        loadHistory();
    }
    
    // valute
    function loadCurrency(){
        CurrencyService.get()
            .then(
                function( curr ) {
                    $scope.currency = curr;
                }
            );
    }
    
    // povijest pretraživanja
    function loadHistory(){
        HistoryService.get()
            .then(
                function( h ) {
                    $scope.history = h;
                }
            );
    }
    
    // vraćanje starog pretraživanja
    function getHistory(id){
        $scope.show  = 2;   // prikaži loading dio
        HistoryService.search(id)
            .then(
                function( h ) {
                    $scope.searchResults    = h;
                    
                    // postavi search polje na ono iz historya
                    $scope.search           = $scope.searchResults.search;
                    
                    // prikaži dio sa rezultatima
                    $scope.show             = 1;    
                }
            );
    }
    
    
    // pretraživanje
    function searchData(){
        $scope.show  = 2;   // prikaži loading dio
        SearchService.search($scope.search)
            .then(
                function( d ) {
                    $scope.searchResults    = d;
                    
                    // prikaži dio sa rezultatima
                    $scope.show             = 1;    
                    
                    // učitaj ponovno povijest
                    loadHistory();
                }
            );
    }
    
    // slanje maila
    function sendEmail(search, email, results, currency, currency_txt){
        if((email == "undefined")||(email == null))
            return;
        if(email.length <= 3)
            return;
        $scope.sendMailTxt = "Šaljem...";
        EmailService.send(search, email, results, currency, currency_txt)
            .then(
                function( e ) {
                    $scope.sendMailTxt = "Poslano";
                }
            );
    }
}]);




