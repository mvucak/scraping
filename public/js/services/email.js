var searchApp = angular.module('searchApp');
searchApp.factory('EmailService', ['$http', function($http) { 
    return({
        send : send
    });
   
    function send(search, email, res, cur, cur_txt){
        var results = "["
        for(var i = 0; i < res.length; i++){
            if(i > 0){
                results += ",";
            }
            results += '{';
            results += '"name":"'+ res[i].name+ '",';
            results += '"price_sum":"'+ (res[i].price_sum * cur) + cur_txt+ '",';
            results += '"provider":"'+ res[i].provider + '"';
            results += '}';
        }
        results += "]";
        
        var data = '{"email":"'+email+'","search":"'+search+'", "results":'+(results)+'}';        
        var request = $http(
            {method: "POST", 
                url: "sendEmail",
            headers: { 'Content-Type': 'application/json' },
               data: data
            }
        );
        
        return( request.then( handleSuccess, handleError ) );
    }
    
    function handleSuccess( response ) {
        return( response.data );
    }
    
    function handleError( response ) {
        return [];
    }
}]);

