var searchApp = angular.module('searchApp');
searchApp.factory('SearchService', ['$http', function($http) { 
    return({
        search : search
    });
   
    function search(val){
        var req     = "/mobile/search/"+val;
        var request = $http(
            {method: "GET", 
                url: req}
        );
        
        return( request.then( handleSuccess, handleError ) );
    }
    
    function handleSuccess( response ) {
        return( response.data );
    }
    
    function handleError( response ) {
        return [];
    }
}]);