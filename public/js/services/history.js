var searchApp = angular.module('searchApp');
searchApp.factory('HistoryService', ['$http', function($http) { 
    return({
        get : get,
        search : search
    });
   
    function get(){
        var request = $http(
            {method: "GET", 
                url: "/history"}
        );
        
        return( request.then( handleSuccess, handleError ) );
    }
    
    function search(id){
        var req     = "/mobile/"+id;
        var request = $http(
            {method: "GET", 
                url: req}
        );
        
        return( request.then( handleSuccess, handleError ) );
    }
    
    function handleSuccess( response ) {
        return( response.data );
    }
    
    function handleError( response ) {
        return [];
    }
}]);