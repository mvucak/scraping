var searchApp = angular.module('searchApp');
searchApp.factory('CurrencyService', ['$http', function($http) { 
    return({
        get : get
    });
   
    function get(){
        var request = $http(
            {method: "GET", 
                url: "/currency"}
        );
        
        return( request.then( handleSuccess, handleError ) );
    }
    
    function handleSuccess( response ) {
        return( response.data );
    }
    
    function handleError( response ) {
        return [];
    }
}]);