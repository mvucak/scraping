<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/
Route::get('/', function() 
{
    return view('index');
});

Route::get('/mobile/search/{search}', 'MobileController@index');        // pretraga 
Route::get('/mobile/{id}', 'MobileController@show');                    // get($id) prikaz konkretne pretrage (iz history-a)
Route::resource('history', 'HistoryController');                        // rezultati starih pretraga
Route::resource('currency', 'CurrencyController');                      // valute
Route::post('sendEmail', 'MailController@index');                       // mail controller
    
    