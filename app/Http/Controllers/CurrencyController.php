<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

class CurrencyController extends Controller
{
    /**
     * Vrati valute u odnosu na Kunu jer su vrijednosi u bazi u kunama
     *
     * @return Response
     */
    public function index()
    {
        $store = new \Illuminate\Cache\ApcStore(new \Illuminate\Cache\ApcWrapper());
        $cache = new \Swap\Cache\IlluminateCache($store, 60);
        
        $httpAdapter = new \Ivory\HttpAdapter\FileGetContentsHttpAdapter();
        // Create the Yahoo Finance provider
        $yahooProvider = new \Swap\Provider\YahooFinanceProvider($httpAdapter);

        // Create Swap with the provider
        $swap = new \Swap\Swap($yahooProvider);
        $eur = $swap->quote('HRK/EUR');
        $chf = $swap->quote('HRK/CHF');
        $currency = array();
        $currency['HRK'] = "1";
        $currency['EUR'] = $eur->getValue();
        $currency['CHF'] = $chf->getValue();
        
        return $currency;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
