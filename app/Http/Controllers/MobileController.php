<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Http\Controllers\Controller;

use App\Mobile;
use App\Scrape;

class MobileController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return Response
     */
    public function index($search)
    {
        $search     = mb_strtolower(trim($search)); 
        // minimalno 2 znaka za pretragu
        if(empty($search) || (mb_strlen($search) < 2)){
           return "";
        }
        
        // učitaj iz baze ako postoje podaci
        $mobile             = Mobile::where("search", "=", $search)->first();
        $time_now           = new \DateTime();
        // ako ne postoje postavi na null
        if(count($mobile) <= 0)
        {
            $mobile             = NULL;
        }
        
        // INAČE napravi novi dohvat podataka s weba
        $s      = new Scrape();
        $phones = $s->getPhones($search);
        
        if(count($phones) > 0)
        {
            if($mobile === NULL)
            {
                $mobile         = new Mobile();
            }
            else 
            {
                $mobile->created_at = $time_now;
            }
            $mobile->updated_at = $time_now;
            $mobile->search     = $search;
            $mobile->results    = $phones;
            
            $mobile->save();
            return $mobile;
        }
        return array();
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @return Response
     */
    public function store()
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function show($id)
    {
        // učitaj iz baze ako postoje podaci
        $mobile             = Mobile::where("_id", "=", $id)->first();
        $minutes_elapsed    = -1;    // vrijeme proteklo od kad je snimljeno u bazu!
        $time_now           = new \DateTime();
        // inače probaj ih pronaći
        if(count($mobile) > 0)
        {
            $time_saved         = new \DateTime($mobile->updated_at);
            $diff               = $time_now->diff($time_saved);
            $hours_elapsed      = $diff->format("%H");
            $minutes_elapsed    = $diff->format("%i");
            $minutes_elapsed    = $minutes_elapsed + ($hours_elapsed * 60);
        }
        else {
            return array();
        }
        
        // ako je proteklo manje od 60 minuta prikaži rezultate iz baze
        if(($minutes_elapsed < 60)&&($minutes_elapsed >= 0))
        {
            return $mobile;
        }
        
        // INAČE napravi novi dohvat podataka s weba
        $s      = new Scrape();
        $phones = $s->getPhones($mobile->search);
        
        if(count($phones) > 0)
        {
            if($mobile === NULL)
            {
                $mobile         = new Mobile();
            }
            else 
            {
                $mobile->created_at = $time_now;
            }
            $mobile->updated_at = $time_now;
            $mobile->results    = $phones;
            
            $mobile->save();
            
            //return Mobile::get();
            return $mobile;
        }
        return array();
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function update($id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return Response
     */
    public function destroy($id)
    {
        //
    }
}
