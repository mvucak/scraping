<?php
namespace App;

class Scrape {
    
    private $phones = array();
    private $search = "";
    
    public function getPhones($search)
    {
        $this->search  = $search;
        $this->tele2();
        
        return $this->phones;
    }
    
    public function tele2()
    {
        $provider       = "tele2";
        $price_monthly  = "200";
        $url            = 'http://www.tele2.hr/privatni-korisnici/mobiteli/uz-pretplatu/';
        $html           = $this->curl_return($url);
        
        # Create a DOM parser object
        $dom            = new \DOMDocument();
        @$dom->loadHTML($html);

        // Uzmi broj stranica
        $elem           = $dom->getElementById ("ucItemsListWithFilter_ucItemsList_ucItemsPaging_lnkLast");
        $pages_number   = $elem->textContent;
        
        // traži konkretan mobitel
        try
        {
            for($k = 1; $k <= $pages_number; $k++) 
            {
                if($k > 1){
                    $html       = $this->curl_return($url."?page=".$k);
                    $dom        = new \DOMDocument();
                    @$dom->loadHTML($html);
                }
                $mobile_menu    = $dom->getElementById ("ucPageHandler_ucItemsListWithFilter_ucItemsListPanel");


                $tables         = $mobile_menu->getElementsByTagName("table");
                $i = 0;
                foreach ($tables as $table){
                    // Nalazi li se string pretrage u tablici
                    $pos = @mb_strpos(mb_strtolower($table->nodeValue), mb_strtolower($this->search));
                    if($pos > 0){
                        
                        // tražimo cijenu, prvo uzimam sve redove tablice
                        $rows = $table->getElementsByTagName("tr");
                        // u drugom je naziv
                        $name = $rows->item(1);
                        // u trećem je cijena
                        $row = $rows->item(3);
                        // u drugom stupcu su vrijednosti pa uzimam njega
                        $cols = $row->getElementsByTagName("td");
                        $item = $cols->item(1);
                        $txt  = $item->textContent;

                        // vadim cijenu bez preplate 
                        $pos = mb_strpos($txt, "cijena bez opcije");
                        $txt = mb_substr($txt, 0, $pos);
                        $txt = trim(mb_substr($txt, $pos-9, 6));
                        $txt = str_replace('.', '', $txt);    
                        $price = $txt ;

                        $price_sum = intval($price) + (intval($price_monthly) * 12);
                        $phone = array("provider" => $provider, "price_sum" => $price_sum, "price_once" => $price, "price_monthly" => $price_monthly, "name" => trim($name->textContent));
                        array_push($this->phones, $phone);
                    }
                }
            }
        }
        catch(Exception $e)
        {
            echo $e->getMessage();
        }
    }
    
    public function curl_return($url)
    {
        $ch = curl_init();
        $timeout = 5;
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_ENCODING, 'UTF-8');
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, $timeout);
        $html = curl_exec($ch);
        curl_close($ch);
        
        return $html;
    }
}
